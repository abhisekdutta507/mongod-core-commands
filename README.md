### MongoClient Documentation

Visit [https://www.mongodb.com/docs/drivers/node/current/](https://www.mongodb.com/docs/drivers/node/current/) for more details.

### Connect your esisting repository

```sh
git remote add origin git@bitbucket.org:abhisekdutta507/mongod-core-commands.git
```

### Clone your repository

```sh
git clone git@bitbucket.org:abhisekdutta507/mongod-core-commands.git
```

### Install MongoDB on Linux from [docs](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/)

```sh
# step 1
wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -

# step 2
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/5.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-5.0.list

# step 3
sudo apt update

# step 4
sudo apt install -y mongodb-org
```

### Start MongoDB through Linux Terminal

```sh
# start MongoDB service
sudo systemctl start mongod

# check MongoDB service status
sudo systemctl status mongod

# stop MongoDB service
sudo systemctl stop mongod
```

### Start MongoDB on Windows Command Prompt as Administrator

```sh
# start MongoDB service
net start MongoDB

# stop MongoDB service
net stop MongoDB
```

### Add Mongo Shell in Environment variables

```
Advanced System Settings -> Environment Variables -> Path -> Add New
```

### Run Mongo Shell from Command Prompt on Windows/Linux

```sh
mongosh
```

### Mac OS Terminal

```sh
# start MongoDB service
sudo mongod
# or
sudo mongod --port 27017

# then run the shell
mongo
# or
mongo --port 27017
```

### Database, Collection & Documents

- In a MongoDB server we can have 1 or more `Databases`.
- Each Database can hold 1 or more `Collections`.
- A `Collection` can be compared as a `Table` in a `SQL Database`.
- Each `Collection` can contain multiple `Documents`.
- `Documents` are the data pieces that we store in a `MongoDB Database`.
- `Documents` & `Collections` are created implicitly. Though we can create them with explicit configuration.
- The default MongoDB [storage engine](https://docs.mongodb.com/manual/core/storage-engines/) in named `WiredTiger`.

### Clear Mongo Shell

```sh
cls
```

### List all Databases

```sh
show dbs
```

### Switch to an exists/non exists Database

```sh
use db_name
# for example
use flights
```

### List all the collections in Database

```sh
show collections
```

### Drop a collection

```sh
db.collection_name.drop()
```

### Drop a Database

```sh
use database_name
db.dropDatabase()
```

### Data Types

- String (max 16MB)
- Boolean
- Number (Int 32, Long/Int 64, Decimal 128)
- ObjectId
- Timestamp
- ISODate

```sh
db.products.insertOne({age: NumberInt(55), price: NumberLong(7489729384792), created: new Date(), updated: new Timestamp(), workTime: NumberDecimal("12.99")})
```

### Insert to a Collection

```json
{
  "departureAirport": "CCU",
  "arrivalAirport": "BLR",
  "aircraft": "Indigo 3315",
  "distance": 12000
}

// or

{
  "_id": "ccu-blr-spice-1176",
  "departureAirport": "CCU",
  "arrivalAirport": "BLR",
  "aircraft": "Spice Jet 1176",
  "distance": 12000
}

// or

[
  {
    "departureAirport": "CCU",
    "arrivalAirport": "BLR",
    "aircraft": "Indigo 3315",
    "distance": 12000
  },
  {
    "departureAirport": "CCU",
    "arrivalAirport": "MAA",
    "aircraft": "Indigo 3915",
    "distance": 16000
  }
]
```

```sh
db.collection_name.insertOne({
  "departureAirport": "CCU",
  "arrivalAirport": "BLR",
  "aircraft": "Indigo 3315"
});

# for example
db.flightData.insertOne({
  "departureAirport": "CCU",
  "arrivalAirport": "BLR",
  "aircraft": "Indigo 3315"
});
# or
db.flightData.insertMany([
  {
    "departureAirport": "CCU",
    "arrivalAirport": "BLR"
  },
  {
    "departureAirport": "BLR",
    "arrivalAirport": "CCU"
  }
]);
```

### Find data

```sh
# Example 1
db.flightData.find();

# Example 2
db.flightData.find().pretty();

# Example 3
db.flightData.findOne();

# Example 4
db.flightData.findOne({ _id: ObjectId("61863369bf4d882a14d51ae9") });

# Example 5
db.flightData.find({ distance: { $gt: 12000 } });

```

### Delete one document

```sh
db.flightData.deleteOne({ "_id": "ccu-blr-spice-1176" });
# or
db.flightData.deleteMany({ "arrivalAirport": "BLR" });
# or
db.flightData.deleteMany({});
```

### Update one document

```sh
db.flightData.updateOne({ "_id": "ccu-blr-spice-1176" }, { $set: { "arrivalAirport": "MAA" } });
# or
db.flightData.updateMany({ "departureAirport": "CCU", "arrivalAirport": "MAA" }, { $set: { "distance": 19000 } });
# or
db.flightData.replaceOne({ "_id": "ccu-blr-spice-1176" }, { "arrivalAirport": "MAA" }, options);
# or similar to replace but can replace multiple documents
db.flightData.update({ "_id": "ccu-blr-spice-1176" }, {"arrivalAirport": "MAA" });
```

### Unset a particular field from documents

The below command will find the documents containing userId ObjectId("6205693e09c31addec0a2f86") and remove the field user ObjectId("6205693e09c31addec0a2f86").

```sh
db.transactions.updateMany(
  {
    userId: ObjectId('6205693e09c31addec0a2f86')
  },
  {
    $unset: {
      user: ObjectId('6205693e09c31addec0a2f86')
    }
  }
);
```

### Atomic [operators](https://www.mongodb.com/docs/manual/reference/operator/query/)

```sh
# $gt (greater than)
# $gte (greater than equals to)
# $lt (lesser than)
# $lte (lesser than equals to)
db.products.findOne({ distance: { $gt: 1000, $lt: 3000 } });
```

```sh
# $exists (finds a document with/without the existing field)
db.products.find({ price: { $exists: true } });
```

```sh
# $elemMatch
db.players.find({ games: { $elemMatch: { score: { $gt: 7 } } } }, { _id: 0, name: 1, games: 1 });
# or
db.players.find({}, { _id: 0, name: 1, games: { $elemMatch: { score: { $gt: 7 } } } });
# both the scenarios have different results
```

```sh
# $slice (finds the documents with n items in array)
db.products.find({ instock: { $slice: 1 } });
# -1 specifies the items from end
#  1 specifies the items from begining
```

### Cursor object

```sh
# does not return all the records from db on shell
# instead it returns a cursor
db.passengers.find();

db.passengers.find().toArray();

db.passenregs.find().forEach();
```

### Projection

```sh
# get only specific fields
db.products.find({}, { departureAirport: 1, _id: 0 })
```

```json
// output
[{ "departureAirport": "CCU" }, { "departureAirport": "BLR" }]
```

```sh
# or only remove a specific field
db.products.find({}, { _id: 0 })
```

```json
// output
[
  { "departureAirport": "CCU", "arrivalAirport": "BLR", "distance": 9000 },
  { "departureAirport": "BLR", "arrivalAirport": "CCU", "distance": 3000 }
]
```

### Embedded Documents/Nested Documents

#### MongoDB supports upto 100 levels of nesting

#### Max 16mb/document

```json
[
  {
    "departureAirport": "CCU",
    "arrivalAirport": "BLR",
    "distance": 9000,
    // embedded documents
    "status": {
      "description": "1 hour late",
      "details": {
        "reason": "Poor weather condition"
      }
    }
  }
]
```

### Array

```json
[
  {
    "name": "Albert",
    // array documents
    "hobies": ["Cooking", "Playing cricket"]
  }
]
```

### Find in embedded documents

```sh
db.products.findOne({
  "status.description": "2 hour late"
});
```

```json
// output
null
// or
{
  "_id": ObjectId("61f19d17fe25daf6c4e1b937"),
  "departureAirport": "CCU",
  "arrivalAirport": "BLR",
  "distance": 9000,
  "status": {
    "description": "1 hour late",
    "details": {
      "reason": "Poor weather condition"
    }
  }
}
```

### Find in Array documents

```sh
db.products.findOne({ hobies: "Cooking" }, { _id: 0 });

# output
null
# or
{
  departureAirport: 'BLR',
  arrivalAirport: 'CCU',
  distance: 3000,
  hobbies: [ 'Cooking', 'Playing cricket' ]
}
```

### Aggregate

Read more about aggregate [here](https://docs.mongodb.com/manual/reference/method/db.collection.aggregate/#db.collection.aggregate--)

Join multiple documents by their `ObjectId` using `$lookup` & `Aggregation`.

```sh
/**
 * @params pipeline {Array}
 * @params options {document}
 */
db.collection.aggregate(pipeline, options);
```

### Aggregate Match

Filter documents using `$match` operator.

```sh
db.collection.aggregate([
  {
    $match: {
      _id: ObjectId('61f840a00e270691501890c7')
    }
  }
]);
```

### Aggregate Projection

Filter documents using `$match` operator & print them using projection. The concept is also known as `select` in `Mongoose`.

```sh
db.collection.aggregate([
  {
    $match: {
      _id: ObjectId('63a462d4df1d7bdc00549f94')
    }
  },
  {
    $project: {
      _id: 0,
      user: 1,
      amount: 1,
      date: 1
    }
  }
]);
```

The above example only shows the `user`, `amount` & `date` fields.

### Aggregate Skip & Limit for paginations

Aggregate queries to perform skip & limit to implement pagination type of response.

```sh
db.orders.aggregate([
  {
    $match: {
      size: 'medium'
    }
  },
  {
    $project: {
      _id: 0,
      name: 1,
      price: 1,
      quantity: 1
    }
  },
  {
    $skip: 3
  },
  {
    $limit: 1
  }
]);
```

### Aggregate Match to filter using $lt or $gt

```sh
db.orders.aggregate([
  {
    $match: {
      size: 'medium',
      quantity: { $gt: 10, $lt: 50 }
    }
  }
]);
```

### Aggregate Lookup

Joins multiple documents together. MongoDB supports mapping of `one to one`, `one to many` & `many to many` documents.

```sh
db.patients.aggregate([
  {
    $lookup: {
      from: "doctors",
      localField: "doctor",
      foreignField: "_id",
      as: "doctor"
    }
  }
]);

# output
[
  {
    _id: ObjectId("61f840a00e270691501890c7"),
    name: 'Abhisek',
    age: 26,
    disease: 'Pain',
    doctor: [ { _id: ObjectId("61f842b40e270691501890c9"), name: 'Tanni' } ]
  },
  {
    _id: ObjectId("61f840f90e270691501890c8"),
    name: 'Arijit',
    age: 40,
    disease: 'Headache',
    doctor: [ { _id: ObjectId("61f842b40e270691501890ca"), name: 'Jay' } ]
  }
]

```

```sh
# insert one record
db.posts.insertOne({
  title: 'My first post',
  text: 'I hope you like it',
  tags: [ 'new', 'tech' ],
  creators: [ ObjectId("61f9966a3cd8e1d08acb2346"), ObjectId("61f9966a3cd8e1d08acb2345") ]
})

# perform filter & population/lookup at the same time
db.posts.aggregate([
  {
    $match: {
      creators: ObjectId("61f9966a3cd8e1d08acb2345")
    }
  },
  {
    $lookup: {
      from: 'creators',
      foreignField: '_id',
      localField: 'creators',
      as: 'creators'
    }
  }
]);
```

Nested level lookup using aggregate. The below aggregation joins the branches collection with users & users with transactions for a bank.

```sh
# command
db.transactions.aggregate([
  {
    $lookup: {
      from: "users",
      foreignField: "_id",
      localField: "userId",
      as: "users",
      pipeline: [
        {
          $project: {
            _id: 0,
            firstName: 1,
            lastName: 1,
            email: 1,
            branchId: 1
          }
        },
        {
          $lookup: {
            from: "branches",
            foreignField: "_id",
            localField: "branchId",
            as: "branches",
            pipeline: [
              {
                $project: {
                  _id: 0,
                  city: 1,
                  state: 1
                }
              }
            ]
          },
        }
      ],
    },
  },
]);

# output
[
  {
    _id: ObjectId("62056ac109c31addec0a2f8c"),
    isCredit: false,
    amount: 500,
    type: 'mortgage',
    userId: ObjectId("6205693e09c31addec0a2f86"),
    users: [
      {
        firstName: 'Abhisek',
        lastName: 'Dutta',
        email: 'abhisek.dutta.507@gmail.com',
        branchId: ObjectId("62056cfb09c31addec0a2f8d"),
        branches: [
          {
            city: 'Kolkata',
            state: 'West Bengal'
          }
        ]
      }
    ]
  }
]
```

We can lookup & fetch like an object instead of an array.

```sh
db.transactions.aggregate([
  {
    $match: {
      _id: ObjectId("62a34cca13659f41e58bcec4")
    }
  },
  {
    $lookup: {
      from: 'users',
      foreignField: '_id',
      localField: 'user',
      as: 'users',
      pipeline: [
        {
          $project: {
            name: 1
          }
        }
      ]
    }
  },
  {
    $lookup: {
      from: 'admins',
      foreignField: '_id',
      localField: 'admin',
      as: 'admins',
      pipeline: [
        {
          $project: {
            name: 1
          }
        }
      ]
    }
  },
  {
    $project: {
      amount: 1,
      user: { $arrayElemAt: ['$users', 0] },
      admin: { $arrayElemAt: ['$admins', 0] }
    }
  }
]);
```

### Aggregate with grouping

Find total amount credited & debited

```sh
db.transactions.aggregate([
  {
    $group: {
      _id: {
        isCredit: '$isCredit'
      },
      amount: {
        $sum: '$amount'
      }
    }
  }
]);

# output
[
  { _id: { isCredit: true }, amount: 17000 },
  { _id: { isCredit: false }, amount: 1500 }
]
```

Find total number of credit & debit transactions by each users

```sh
db.transactions.aggregate([
  {
    $group: {
      _id: {
        isCredit: '$isCredit',
        userId: ObjectId("6205693e09c31addec0a2f86")
      },
      noOfTransactions: {
        $count: {}
      }
    }
  }
]);

# output
[
  {
    _id: { isCredit: true, userId: ObjectId("6205693e09c31addec0a2f86") },
    noOfTransactions: 3
  },
  {
    _id: { isCredit: false, userId: ObjectId("6205693e09c31addec0a2f86") },
    noOfTransactions: 2
  }
]
```

Find total cost & quantity of the ordered items.

```sh
db.orders.aggregate([
  {
    $group: {
      _id: {
        size: '$size',
        price: '$price'
      },
      count: {
        $count: {}
      },
      cost: {
        $sum: {
          $multiply: [
            '$price',
            '$quantity'
          ]
        }
      },
      quantity: {
        $sum: '$quantity'
      }
    }
  },
  {
    $project: {
      _id: 0,
      size: '$_id.size',
      unitPrice: '$_id.price',
      count: 1,
      cost: 1,
      quantity: 1
    }
  }
]);

# output

[
  {
    count: 1,
    cost: 140,
    quantity: 10,
    size: 'large',
    unitPrice: 14
  },
  {
    count: 1,
    cost: 180,
    quantity: 10,
    size: 'medium',
    unitPrice: 18
  }
]
```

### MongoDB Transactions

Create a session to start transactions in MongoDB.

```js
import { MongoClient, ServerApiVersion } from 'mongodb';

// Create a MongoClient with a MongoClientOptions object to set the Stable API version
const client = new MongoClient(uri, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: false
  }
});

function createReservationDocument(name = '', dates = [], reservationDetails = {}) {
  return {
    name,
    dates,
    ...reservationDetails
  };
}

async function createReservation(nameOfListing = '', reservationDates = [], reservationDetails = {}) {
  const users = client.db('airbnb').collection('users');
  const reservation = createReservationDocument(nameOfListing, reservationDates, reservationDetails);

  const session = client.startSession();
  const transactionOptions = {
    readPreference: 'primary',
    readConcern: { level: 'local' },
    writeConcern: { w: 'majority' }
  };

  try {
    const transactionResults = await session.withTransaction(async () => {
      const userIsUpdated = await users.updateOne(
        { email: 'leslie@example.com' },
        { $addToSet: { reservations: reservation } },
        { session }
      );

      const foundUsers = await users.findOne({  email: 'leslie@example.com' }, { session });
      // here we will see the user with email leslie@example.com will have the updated data
      await session.abortTransaction();
      // we aborted the transaction
    });
    const foundUsers = await users.findOne({  email: 'leslie@example.com' }, { session });
    // here we will see the old data. because the session is aborted

    if(transactionResults) {
      // here we can check the transaction status
    }
  } catch(error) {

  } finally {
    await session.endSession();
  }
}
```

Error handling in transaction.

```js
async function createReservation(nameOfListing = '', reservationDates = [], reservationDetails = {}) {
  // ...

  try {
    const transactionResults = await session.withTransaction(async () => {
      const userIsUpdated = await users.updateOne(
        { email: 'leslie@example.com' },
        { $addToSet: { reservations: reservation } },
        { session }
      );
      if (userIsUpdated.matchedCount === 0) {
        await session.abortTransaction();
        throw Error(`failed to find email: ${email} in users`);
      }

      const updatedListing = await listingsAndReviews.updateOne({ name: reservation.name }, { $set: { datesReserved: reservation.dates } }, { session });
      if (updatedListing.matchedCount === 0) {
        await session.abortTransaction();
        throw Error(`failed to find name: ${reservation.name} in listingsAndReviews`);
      }
    });

    // when error is thrown the below code will not execute
    if (transactionResults.ok) {

    }
  catch(error) {
    // error.message will be `Error: failed to find name Snow Lion in listingsAndReviews`
  } finally {
    await session.endSession();
  }
}
```

Transaction result looks like this. When commited. When failed it is `undefined`.

```json
{
  "ok": 1,
  "$clusterTime: {
    "clusterTime": new Timestamp({ t: 1690431151, i: 12 }),
    "signature": {
      "hash": Binary.createFromBase64("2LhBLFYRLCfSapJSKItr0akbBY8=", 0),
      "keyId": new Long("7215615275700322306")
    }
  },
  "operationTime": new Timestamp({ t: 1690431151, i: 10 })
}
```

The `updateOne` response looks like this.

```json
{
  "acknowledged": true,
  "matchedCount": 1,
  "modifiedCount": 1,
  "upsertedCount": 0,
  "upsertedId": null // ObjectId or null
}
```

### Schema validation

Create a simple schema validation

```sh
db.createCollection("posts", {
  validator: {
    $jsonSchema: {
      bsonType: "object",
      required: ["title", "text", "creator", "comments"],
      properties: {
        title: {
          bsonType: "string",
          description: "must be a string and is required",
        },
        text: {
          bsonType: "string",
          description: "must be a string and is required",
        },
        creator: {
          bsonType: "objectId",
          description: "must be an objectid and is required",
        },
        comments: {
          bsonType: "array",
          description: "must be an array and is required",
          items: {
            bsonType: "object",
            required: ["text", "author"],
            properties: {
              text: {
                bsonType: "string",
                description: "must be a string and is required",
              },
              author: {
                bsonType: "objectId",
                description: "must be an objectid and is required",
              },
            },
          },
        },
      },
    },
  },
});
```

Update an existing schema validation

```sh
db.runCommand({
  collMod: "posts",
  validator: {
    $jsonSchema: {
      bsonType: "object",
      required: ["title", "text", "creator", "comments"],
      properties: {
        title: {
          bsonType: "string",
          description: "must be a string and is required",
        },
        text: {
          bsonType: "string",
          description: "must be a string and is required",
        },
        creator: {
          bsonType: "objectId",
          description: "must be an objectid and is required",
        },
        comments: {
          bsonType: "array",
          description: "must be an array and is required",
          items: {
            bsonType: "object",
            required: ["text", "author"],
            properties: {
              text: {
                bsonType: "string",
                description: "must be a string and is required",
              },
              author: {
                bsonType: "objectId",
                description: "must be an objectid and is required",
              },
            },
          },
        },
      },
    },
  },
  validationAction: "warn",
});
```

### Performance Improvement topics in MongoDB

#### createIndex

An Index is an array of poiters. That stored the address of the documents to be searched.

```sh
db.transactions.explain('executionStats').find({ isCredit: true });
```

The above command explain the query & the time taken to execute the query. With the help of index we can reduce the execution time under certain conditions.

#### Capped Collection

Creates a collection named `logs` and allows to store max 10 documents in the collection & total document size will be allowed 1000 bytes. After 10 the old documents will be automatically removed.

```sh
db.createCollection('logs', { capped: true, size: 1000, max: 10 });
```

#### Replica Sets

Reader instances.

#### Sharding

Distributed documents comes with Sharding Key.
