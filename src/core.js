import { MongoClient } from 'mongodb';
import { DB } from './constant.js';

export class Core {
  constructor(collection = '', connector = { client: new MongoClient() }) {
    this.collection = connector.client.db(DB).collection(collection);
  }

  async insertOne(document = {}, options = {}) {
    return await this.collection.insertOne(document, options);
  }

  async insertMany(document =[], options = {}) {
    return await this.collection.insertMany(document, options);
  }

  async findOne(match = {}, options = {}) {
    return await this.collection.findOne(match, options);
  }

  async find(match = {}, options = {}) {
    return await this.collection.find(match, options).toArray();
  }

  async updateOne(match = {}, dataToBeUpdated = {}, options = {}) {
    return await this.collection.updateOne(match, dataToBeUpdated, options)
  }

  async aggregate(pipeline = [], options = {}) {
    return await this.collection.aggregate(pipeline, options).toArray();
  }
}
