import * as readline from 'node:readline/promises';
import { stdin as input, stdout as output } from 'node:process';

export class CommandLine {
  _cmd = readline.createInterface({ input, output });

  constructor() {}

  async question(question = '? ') {
    return await this._cmd.question(question);
  }

  close() {
    this._cmd.close();
  }
}
