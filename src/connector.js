import { MongoClient, ServerApiVersion } from 'mongodb';
import { URI } from './constant.js';

class Connector {
  constructor(uri) {
    // Create a MongoClient with a MongoClientOptions object to set the Stable API version
    this.client = new MongoClient(uri, {
      serverApi: {
        version: ServerApiVersion.v1,
        strict: true,
        deprecationErrors: false
      }
    });
  }

  async connect() {
    try {
      // Connect the client to the server (optional starting in v4.7)
      await this.client.connect();

      // Send a ping to confirm a successful connection
      // await this.client.db('admin').command({ ping: 1 });
    } finally {
      await this.client.close();
    }
  }
}

export const connector = new Connector(URI);
