import { ObjectId } from 'mongodb';
import { Core } from './core.js';
import { connector } from './connector.js';
import { COLLECTION, YES } from './constant.js';
import { CommandLine } from './readline.js';

class Users extends Core {
  constructor() {
    super(COLLECTION.users, connector);
  }
}

class ListingsAndReviews extends Core {
  constructor() {
    super(COLLECTION.listingsAndReviews, connector);
  }
}

function createReservationDocument(name = '', dates = [], reservationDetails = {}) {
  return {
    name,
    dates,
    ...reservationDetails
  };
}


async function createReservation(nameOfListing = '', reservationDates = [], reservationDetails = {}) {
  const users = new Users();
  const listingsAndReviews = new ListingsAndReviews();
  const reservation = createReservationDocument(nameOfListing, reservationDates, reservationDetails);

  const session = connector.client.startSession();
  const transactionOptions = {
    readPreference: 'primary',
    readConcern: { level: 'local' },
    writeConcern: { w: 'majority' }
  };

  try {
    const transactionResults = await session.withTransaction(async () => {
      const email = 'leslie@example.com';
      const userIsUpdated = await users.updateOne(
        { email },
        { $addToSet: { reservations: reservation } },
        { session }
      );
      if (userIsUpdated.matchedCount === 0) {
        await session.abortTransaction();
        // throw Error(`Failed to find email: ${email} in users`);
      }

      const updatedListing = await listingsAndReviews.updateOne({ name: reservation.name }, { $set: { datesReserved: reservation.dates } }, { session });
      if (updatedListing.matchedCount === 0) {
        await session.abortTransaction();
        // throw Error(`Failed to find name: ${reservation.name} in listingsAndReviews`);
      }
    }, transactionOptions);

    console.log('@debugger - transactionResults', transactionResults);

    if (transactionResults) {

    }
  } catch(error) {
    console.log('@debugger -', error.message);
  } finally {
    await session.endSession();
  }
}

async function main() {
  const cmd = new CommandLine();
  let continueExecution = YES;

  do {
    const nameOfListing = await cmd.question('Name of Listing? = ');
    const reservationDates = [new Date()];
    const otherDetails = await cmd.question('Reservation Details? = ');
    const reservationDetails = JSON.parse(otherDetails);
    await connector.connect.bind(this, connector);
    await createReservation(nameOfListing, reservationDates, reservationDetails);

    continueExecution = await cmd.question('\ndo you want to continue? (yes/no) = ');
  } while(continueExecution === YES);

  cmd.close();
}

main();
