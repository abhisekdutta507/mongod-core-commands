import { config } from 'dotenv';

config();

export const USER_NAME = process.env.USER_NAME;
export const PASSWORD = process.env.PASSWORD;
export const HOST = process.env.HOST;
export const URI = `mongodb+srv://${USER_NAME}:${PASSWORD}@${HOST}/`;
export const DB = 'airbnb';
export const COLLECTION = {
  users: 'users',
  listingsAndReviews: 'listingsAndReviews'
};
export const YES = 'yes';
export const NO = 'no';
